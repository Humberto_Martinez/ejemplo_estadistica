﻿int[] valores = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
int tamano = valores.Length;

//Dar formato al arreglo

for(int i=0; i<valores.Length; i++)
{
    Console.Write("[" + valores[i] + "]");
    if(i < valores.Length -2)
        Console.Write(",");
    if (i == tamano - 2)
        Console.Write(" Y ");
    if (i == tamano - 1)
        Console.Write(".");
}

Console.WriteLine("\n");

//Suma de todos los elementos de los arreglos
int TOTAL = 0;

for(int i=0; i < valores.Length; i++)
{
    TOTAL = TOTAL + valores[i];
}
Console.WriteLine("El TOTAL es: " + TOTAL);

Console.WriteLine("\n");



//Promedio de los elementos del arreglo
int TOTAL_ARRAY = valores.Length;
double PROMEDIO;

PROMEDIO = TOTAL / TOTAL_ARRAY;
Console.WriteLine("El PROMEDIO es: " + PROMEDIO);

Console.WriteLine("\n");



//Varianza
double ACUMULADO = 0;
double VARN = 0;

for(int i=0;i<valores.Length; i++)
{
    ACUMULADO = ACUMULADO + Math.Pow(valores[i] - PROMEDIO, 2);
}

VARN = ACUMULADO / (tamano - 1);

Console.WriteLine("La VARIANZA es: " + VARN);

Console.WriteLine("\n");



//Desviación estándar
double SUMATORIA = 0;
double DESV = 0;

for(int i = 0; i < valores.Length; i++)
{
    SUMATORIA = SUMATORIA + Math.Pow(valores[i] - PROMEDIO, 2);
}

DESV = Math.Sqrt(SUMATORIA / (tamano-1));

Console.WriteLine("La DESVIACIÓN ESTÁNDAR es: " + DESV);

Console.WriteLine("\n");




//Mediana

//Ordenando los números
Array.Sort(valores);

double MEDIANA;
int MITAD = tamano / 2;

if (valores.Length % 2 == 0)
{
    MEDIANA = (((MITAD + 1) + MITAD) / 2.0);
    Console.WriteLine("La MEDIANA es: " + MEDIANA);
}
else
{
    MEDIANA = (valores[MITAD]);
    Console.WriteLine("La MEDIANA es: " + MEDIANA);
}

Console.WriteLine("\n");





//Moda

int MODA = valores[0];
int NUMERO = 0;

for(int i = 0; i < valores.Length; i++)
{
    int REPETIR = 0;
    for(int j = 0;j < valores.Length; j++)
    {
        if (valores[j] == valores[i]) REPETIR++;
    }
    if (REPETIR > NUMERO)
    {
        MODA = valores[i];
        NUMERO = REPETIR;
    }
}
Console.WriteLine("La MODA es: " + MODA);

Console.WriteLine("\n");



//Cuartiles
double Q1 = 0;
double Q2 = 0;
double Q3 = 0;

Array.Sort(valores);

for(int i = 0;i < valores.Length; i++)
{
    Q1 = Math.Round(TOTAL_ARRAY * 0.25);

    Q2 = Math.Round(TOTAL_ARRAY * 0.50);

    Q3 = Math.Round(TOTAL_ARRAY * 0.75);

    if (Q1 == valores[i])
    {
        Console.WriteLine("El CUARTIL 1 se encuentra en la posición " + Q1 + " y es el dato " + valores[i]);
    }

    if(Q2 == valores[i])
    {
        Console.WriteLine("El CUARTIL 2 se encuentra en la posición " + Q2 + " y es el dato " + valores[i]);
    }

    if (Q3 == valores[i])
    {
        Console.WriteLine("El CUARTIL 3 se encuentra en la posición " + Q3 + " y es el dato " + valores[i]);
    }
}

Console.WriteLine("\n");



//Coeficiente de variación de Pearson

double COEFICIENTE = 0;

COEFICIENTE = DESV / PROMEDIO;
Console.WriteLine("El COEFICIENTE DE VARIACIÓN DE PEARSON es: " + COEFICIENTE);
